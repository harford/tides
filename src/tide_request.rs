use chrono::DateTime;
use chrono::Datelike;
use chrono::Duration;
use chrono::SecondsFormat;
use chrono::TimeZone;
use chrono::Utc;
use chrono_tz::Tz;
use chs::apis::{configuration, data_api};
use crate::station;

/// Handles a command starting with "tide "
///
/// Takes a string that starts with "tide " and parses it. The general form is "tide <station>
/// [date]" where `date` can be empty, or today/tomorrow/yesterday, or a YYYY/MM/DD string.
///
/// It will return a string to be passed back to the user, which may be an error message, or it may
/// be a valid list of tides.
pub async fn handle_tides(
    command: &str,
    configuration: &configuration::Configuration,
) -> String {
    let params: Vec<&str> = command.split(' ').collect();

    let mut date = "";

    if params.len() == 3 {
        date = params[2];
    }

    let station_code = params[1];

    let station = match station::get_station_info(station_code) {
        Ok(station) => station,
        Err(_) => return print_station_not_found(station_code),
    };

    let date = match parse_date(date, station.timezone) {
        Ok(date) => date,
        Err(bad_date) => return bad_date,
    };

    let tides = get_tides(date, station.id, station.timezone, configuration).await;

    format_tides(tides, station.code, station.name, &date)
}

fn print_station_not_found(code: &str) -> String {
    format!("No station found with code '{}'", code)
}

fn format_tides(tides: Vec<(String, f64)>, code: &str, name: &str, date: &DateTime<Tz>) -> String {
    let mut response = format!(
        "Tides for {} ({}) on {}",
        name,
        code,
        date.format("%Y/%m/%d - %Z")
    );

    for (date, tide) in tides {
        response.push_str(&format!("\n{}: {:.1}m", date, tide));
    }

    response
}

async fn get_tides(
    start: chrono::DateTime<Tz>,
    station_id: &str,
    timezone: &str,
    configuration: &configuration::Configuration,
) -> Vec<(String, f64)> {
    let end = start + Duration::hours(24);

    // Get the tides
    let tides = data_api::get_data(
        configuration,
        station_id,
        &start
            .with_timezone(&Utc)
            .to_rfc3339_opts(SecondsFormat::Secs, true),
        &end.with_timezone(&Utc)
            .to_rfc3339_opts(SecondsFormat::Secs, true),
        Some("wlp-hilo"),
    )
    .await;

    let mut result = Vec::new();

    let tz: Tz = timezone.parse().unwrap();
    for tide in tides.unwrap() {
        let utc = chrono::DateTime::parse_from_rfc3339(&tide.event_date.unwrap()).unwrap();
        let date = utc.with_timezone(&tz).format("%H:%M").to_string();
        result.push((date, tide.value.unwrap()));
    }

    result
}

fn get_day_start(current: chrono::DateTime<Utc>, timezone: &str) -> chrono::DateTime<Tz> {
    let tz: Tz = timezone.parse().unwrap();
    let now = current.with_timezone(&tz);
    tz.ymd(now.year(), now.month(), now.day()).and_hms(0, 0, 0)
}

fn today(timezone: &str) -> DateTime<Tz> {
    let now = Utc::now();
    get_day_start(now, timezone)
}

fn parse_date(date: &str, timezone: &str) -> Result<DateTime<Tz>, String> {
    if date == "today" || date.is_empty() {
        Ok(today(timezone))
    } else if date == "yesterday" {
        Ok(today(timezone) - Duration::days(1))
    } else if date == "tomorrow" {
        Ok(today(timezone) + Duration::days(1))
    } else {
        let tz: Tz = timezone.parse().unwrap();
        let naive_date = match chrono::NaiveDate::parse_from_str(date, "%Y/%m/%d") {
            Err(_) => {
                let err_string = format!("Failed to parse date: {}", date);
                return Err(err_string);
            }
            Ok(naive_date) => naive_date,
        };

        Ok(
            tz.ymd(naive_date.year(), naive_date.month(), naive_date.day())
            .and_hms(0, 0, 0)
          )
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono_tz::America::Vancouver;

    #[tokio::test]
    async fn test_handle_invalid_message() {
    }

    #[test]
    fn test_get_day_start() {
        // Make sure the timezone gets applied
        let dt = Utc.ymd(2000, 6, 15).and_hms(0, 5, 30);
        let start = get_day_start(dt, "America/Vancouver");
        assert_eq!(start.date().year(), 2000);
        assert_eq!(start.date().month(), 6);
        assert_eq!(start.date().day(), 14);
        assert_eq!(start.time().format("%H:%M:%S").to_string(), "00:00:00");

        let dt = Utc.ymd(2000, 6, 15).and_hms(10, 5, 30);
        let start = get_day_start(dt, "America/Vancouver");
        assert_eq!(start.date().year(), 2000);
        assert_eq!(start.date().month(), 6);
        assert_eq!(start.date().day(), 15);
        assert_eq!(start.time().format("%H:%M:%S").to_string(), "00:00:00");
    }

    #[test]
    fn test_parse_invalid_date() {
        let cooked = parse_date("asdf", "America/Vancouver");
        assert!(cooked.is_err());
    }

    #[test]
    fn test_parse_empty_date() {
        let cooked = parse_date("", "America/Vancouver");

        let dt = Utc::now().with_timezone(&Vancouver);
        let start = dt.date().and_hms(0,0,0);

        assert_eq!(cooked.unwrap(), start);
    }

    #[test]
    fn test_parse_today() {
        let cooked = parse_date("today", "America/Vancouver");

        let dt = Utc::now().with_timezone(&Vancouver);
        let start = dt.date().and_hms(0,0,0);

        assert_eq!(cooked.unwrap(), start);
    }

    #[test]
    fn test_parse_yesterday() {
        let cooked = parse_date("yesterday", "America/Vancouver");

        let dt = Utc::now().with_timezone(&Vancouver) - Duration::hours(24);
        let start = dt.date().and_hms(0,0,0);

        assert_eq!(cooked.unwrap(), start);
    }

    #[test]
    fn test_parse_tomorrow() {
        let cooked = parse_date("tomorrow", "America/Vancouver");

        let dt = Utc::now().with_timezone(&Vancouver) + Duration::hours(24);
        let start = dt.date().and_hms(0,0,0);

        assert_eq!(cooked.unwrap(), start);
    }

    #[test]
    fn test_parse_specific_date() {
        let naive_start = chrono::NaiveDate::from_ymd(2021, 1, 1).and_hms(0,0,0);
        let start = Vancouver.from_local_datetime(&naive_start).unwrap();

        let cooked = parse_date("2021/1/1", "America/Vancouver");
        assert_eq!(cooked.unwrap(), start);

        let cooked = parse_date("2021/01/1", "America/Vancouver");
        assert_eq!(cooked.unwrap(), start);

        let cooked = parse_date("2021/1/01", "America/Vancouver");
        assert_eq!(cooked.unwrap(), start);
    }
}
