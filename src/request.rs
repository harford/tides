use chs::apis::configuration;

use crate::station_request;
use crate::tide_request;

pub async fn handle_request(
    req: &str,
    configuration: &configuration::Configuration,
) -> String {
    let cooked = req.to_lowercase();

    if cooked.starts_with("station ") {
        return station_request::handle_stations(req);
    }

    if cooked.starts_with("tide ") {
        return tide_request::handle_tides(req, configuration).await;
    }

    print_help()
}

fn print_help() -> String {
    let messages = vec![
        "⛵ Canadian Tides ⛵",
        "🌊🌊🌊🌊🌊🌊🌊🌊🌊🌊",
        "",
        "tide <station> [date]",
        "station search <text>",
        "station info <station code>",
    ];

    messages.join("\n")
}

#[cfg(test)]
mod test {
    use super::*;
    use chs::apis::configuration;

    #[tokio::test]
    async fn test_handle_invalid_message() {
        let configuration = configuration::Configuration::new();
        let message = handle_request("invalid", &configuration).await;

        assert!(message.starts_with("⛵ Canadian Tides ⛵"));
    }
}
