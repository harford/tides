//use async_std::task;
use worker::*;

pub mod request;
pub mod station;
mod station_request;
mod tide_request;

#[event(fetch)]
pub async fn main(mut req: Request, _env: Env) -> Result<Response> {
    if !matches!(req.method(), Method::Post) {
        return Response::error("Method Not Allowed", 405);
    }

    if let Some(body) = req.form_data().await?.get("Body") {
        console_log!("found body");
        return match body {
            FormEntry::Field(buf) => {
                handle_query(&buf).await
            }
            _ => Response::error("`body` part of POST form must be a field", 400),
        };
    }

    console_log!("didn't find body");
    Response::error("Bad Request", 400)
}

async fn handle_query(body: &str) -> Result<Response> {
    let configuration = chs::apis::configuration::Configuration::new();
    let result = request::handle_request(body, &configuration).await;
    Response::ok(result)
}
