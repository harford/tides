use async_std::task;
//use std::error::Error;
use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use std::{thread,time};


fn main() {
    task::block_on(get_stations())
}

#[derive(Serialize, Deserialize)]
struct Station {
    name: String,
    timezone: String,
    code: String,
    lat: f64,
    lng:  f64,
}

fn get_timezone(lat: f64, lon: f64) -> String {
    thread::sleep(time::Duration::from_secs(1));

    eprintln!("lat: {} lng: {}", lat, lon);

    let url = "http://api.geonames.org/timezoneJSON";
    let params = [
        ("username", "alexjh"),
        ("lat", &lat.to_string()),
        ("lng", &lon.to_string()),
    ];
    let url = reqwest::Url::parse_with_params(url, &params).unwrap();
    let res = reqwest::blocking::get(url);

    let res_data = res.unwrap().json::<serde_json::Value>();
    eprintln!("{:?}", res_data);
    
    let zone = res_data.unwrap();
    return String::from(zone["timezoneId"].as_str().unwrap_or("UTC"));
}

async fn get_stations() {
    /* Gets the station list from CHS and returns the data in JSON format 
     *
     * {
     *  id: [name, code, timezone],
     * }
     * 
     */

    let configuration = chs::apis::configuration::Configuration::new();

    let chs_stations = chs::apis::stations_api::get_stations(&configuration, None, None, None).await;

    let station_list = match chs_stations {
        Ok(stations) => stations,
        Err(_error) => Vec::new(),
    };

    let mut stations = HashMap::new();

    for station in station_list.iter() {
        if station.id != None && station.code != None && station.official_name != None {
            let timezone = get_timezone(station.latitude.unwrap(), station.longitude.unwrap());
            eprintln!("{:?}", timezone);
            stations.insert(station.id.clone().unwrap(),
                Station{code: station.code.clone().unwrap(), name: station.official_name.clone().unwrap(), timezone, lat: station.latitude.unwrap(), lng: station.longitude.unwrap()});
        }
    }

    let j = serde_json::to_string(&stations);

    match j {
        Ok(j) => println!("{}", j),
        Err(error) => println!("problem: {:?}", error),
    }
}
