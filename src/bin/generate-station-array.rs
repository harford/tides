use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, std::fmt::Debug)]
struct Station {
    name: String,
    timezone: String,
    code: String,
    lat: f64,
    lng: f64,
}

fn main() {
    /* Example station
    {
      "5cebf1e33d0f4a073c4bc2d3": {
        "name": "Cap-d'Espoir",
        "timezone": "America/Toronto",
        "code": "02290",
        "lat": 48.414713,
        "lng": -64.327389
      }
    }
      */
    let data = std::fs::read_to_string("stations1.json").expect("Can't read stations1.json");

    let stations: std::collections::HashMap<String, Station> = serde_json::from_str(&data).unwrap();

    let mut output = String::from("let stations: std::collections::HashMap<String, Station> = std::collections::HashMap::from([\n");

    /*
    let stations: std::collections::HashMap<String, Station> = std::collections::HashMap::from([
    (
        String::from("5cebf1e33d0f4a073c4bc2d3"),
        Station {
            name: String::from("Cap-d'Espoir"),
            timezone: String::from("Cap-d'Espoir"),
            code: String::from("Cap-d'Espoir"),
            lat: 48.414713,
            lng: -64.327389,
        },
    )
    ]);
    */

    output.push_str(&format!(
        "static STATION_DB: [Station;{}] = [\n",
        stations.len()
    ));

    for (key, station) in stations {
        output.push_str("Station {\n");
        output.push_str("name: \"");
        output.push_str(&station.name);
        output.push_str("\",\n");
        output.push_str("id: \"");
        output.push_str(&key);
        output.push_str("\",\n");
        output.push_str("timezone: \"");
        output.push_str(&station.timezone);
        output.push_str("\",\n");
        output.push_str("code: \"");
        output.push_str(&station.code);
        output.push_str("\",\n");
        output.push_str("lat: ");
        let lat = format!("{:.3}", station.lat);
        output.push_str(&lat);
        output.push_str(",\n");
        output.push_str("lng: ");
        let lng = format!("{:.3}", station.lng);
        output.push_str(&lng);
        output.push_str(",\n");
        output.push_str("},\n");
    }

    output.push_str("\n];\n");

    print!("{}", output);
}
