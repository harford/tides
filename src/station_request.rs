use crate::station;

pub fn handle_stations(command: &str) -> String {
    if command.starts_with("station info ") {
        return handle_station_info(command);
    }
    if command.starts_with("station search ") {
        return handle_station_search(command);
    }

    print_station_help()
}

fn print_station_help() -> String {
    "Command not recognized".to_string()
}

fn handle_station_info(command: &str) -> String {
    let params: Vec<&str> = command.split(' ').collect();

    if params.len() != 3 {
        return print_station_help();
    }

    let info = station::get_station_info(params[2]).unwrap();

    let mut response: String = "Station Details:\n".to_string();

    response.push_str(&format!("Name: {}\n", info.name));
    response.push_str(&format!("Code: {}\n", info.code));
    response.push_str(&format!("Timezone: {}\n", info.timezone));
    response.push_str(&format!("Lat: {}\n", info.lat));
    response.push_str(&format!("Lng: {}\n", info.lng));

    response
}

fn handle_station_search(command: &str) -> String {
    let params: Vec<&str> = command.splitn(3, ' ').collect();

    if params.len() != 3 {
        return print_station_help();
    }

    let found_stations: Vec<&station::Station> = station::find_stations(params[2]);
    if found_stations.len() > 10 {
        return format!(
            "Found {} matching stations. Please be more specific with your search",
            found_stations.len()
        );
    }

    let mut response: String = "Found Stations:".to_string();

    let mut max_len = 0;

    for station in found_stations.iter() {
        if station.name.len() > max_len {
            max_len = station.name.len();
        }
    }

    for station in found_stations.iter() {
        response.push_str(&format!(
            "\n{:max_len$} / {}",
            station.name,
            station.code,
            max_len = max_len
        ));
    }

    response
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_handle_invalid_message() {
        let message = handle_stations("invalid");

        assert_eq!(message, "Command not recognized".to_string());
    }

    #[test]
    fn test_handle_invalid_info() {
        let message = handle_station_info("invalid");

        assert_eq!(message, "Command not recognized".to_string());
    }

    #[test]
    fn test_handle_invalid_search() {
        let message = handle_station_search("invalid");

        assert_eq!(message, "Command not recognized".to_string());
    }

    #[test]
    fn test_handle_station_info() {
        let message = handle_station_info("station info 07755");

        let expected = [
            "Station Details:",
            "Name: Port Moody",
            "Code: 07755",
            "Timezone: America/Vancouver",
            "Lat: 49.288",
            "Lng: -122.866\n",
        ]
        .join("\n");

        assert_eq!(message, expected);
    }

    #[test]
    fn test_handle_station_search() {
        let message = handle_station_search("station search moody");
        let expected = ["Found Stations:", "Port Moody / 07755"].join("\n");

        assert_eq!(message, expected);
    }

    #[test]
    fn test_handle_station_search_with_too_many_matches() {
        let message = handle_station_search("station search a");
        let expected = "Found 734 matching stations. Please be more specific with your search";

        assert_eq!(message, expected);
    }
}
