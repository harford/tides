use pact_consumer::prelude::*;

/* TODO
 *
 * - parameterize the tests
 * - test all timezones?
 * - use json in body and serialize it
 *
 */

#[tokio::test]
async fn tide_request_uppercase() {
    let pact = PactBuilder::new("Consumer", "Test Service")
        .interaction("a retrieve tide request during PST", "", |mut i| async move {
            i.request.path("/tides/api/v1/stations/5cebf1de3d0f4a073c4bb94a/data")
                .query_param("time-series-code", "wlp-hilo")
                .query_param("from", "2021-02-06T08:00:00Z")
                .query_param("to", "2021-02-07T08:00:00Z");
            i.response
                .content_type("application/json")
                .body("[{\"eventDate\":\"2021-02-06T08:08:00Z\",\"qcFlagCode\":\"1\",\"value\":0.903,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-06T14:14:00Z\",\"qcFlagCode\":\"1\",\"value\":3.825,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-06T20:12:00Z\",\"qcFlagCode\":\"1\",\"value\":2.793,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-07T02:18:00Z\",\"qcFlagCode\":\"1\",\"value\":7.661,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-07T06:21:00Z\",\"qcFlagCode\":\"1\",\"value\":3.62,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}] ");
            i
        })
    .await
        .start_mock_server();


    let mut configuration = chs::apis::configuration::Configuration::new();
    configuration.base_path = pact.path("/tides").to_string();
    let body = "TIDE 07755 2021/02/06"; // " 2020/06/1";
    let result = tides::request::handle_request(body, &configuration).await;
    let expected = [
        "Tides for Port Moody (07755) on 2021/02/06 - PST",
        "00:08: 0.9m",
        "06:14: 3.8m",
        "12:12: 2.8m",
        "18:18: 7.7m",
        "22:21: 3.6m",
    ].join("\n");
    assert_eq!(result, expected);
}

#[tokio::test]
async fn tide_request_during_pst_on_specific_date() {
    let pact = PactBuilder::new("Consumer", "Test Service")
        .interaction("a retrieve tide request during PST", "", |mut i| async move {
            i.request.path("/tides/api/v1/stations/5cebf1de3d0f4a073c4bb94a/data")
                .query_param("time-series-code", "wlp-hilo")
                .query_param("from", "2021-02-06T08:00:00Z")
                .query_param("to", "2021-02-07T08:00:00Z");
            i.response
                .content_type("application/json")
                .body("[{\"eventDate\":\"2021-02-06T08:08:00Z\",\"qcFlagCode\":\"1\",\"value\":0.903,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-06T14:14:00Z\",\"qcFlagCode\":\"1\",\"value\":3.825,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-06T20:12:00Z\",\"qcFlagCode\":\"1\",\"value\":2.793,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-07T02:18:00Z\",\"qcFlagCode\":\"1\",\"value\":7.661,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2021-02-07T06:21:00Z\",\"qcFlagCode\":\"1\",\"value\":3.62,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}] ");
            i
        })
    .await
        .start_mock_server();


    let mut configuration = chs::apis::configuration::Configuration::new();
    configuration.base_path = pact.path("/tides").to_string();
    let body = "tide 07755 2021/02/06"; // " 2020/06/1";
    let result = tides::request::handle_request(body, &configuration).await;
    let expected = [
        "Tides for Port Moody (07755) on 2021/02/06 - PST",
        "00:08: 0.9m",
        "06:14: 3.8m",
        "12:12: 2.8m",
        "18:18: 7.7m",
        "22:21: 3.6m",
    ].join("\n");
    assert_eq!(result, expected);
}

#[tokio::test]
async fn tide_request_during_pdt_on_specific_date() {
    let pact = PactBuilder::new("Consumer", "Test Service")
        .interaction("a retrieve tide request during PDT", "", |mut i| async move {
            i.request.path("/tides/api/v1/stations/5cebf1de3d0f4a073c4bb94a/data")
                .query_param("time-series-code", "wlp-hilo")
                .query_param("from", "2020-10-01T07:00:00Z")
                .query_param("to", "2020-10-02T07:00:00Z");
            i.response
                .content_type("application/json")
                .body("[{\"eventDate\":\"2020-10-01T08:08:00Z\",\"qcFlagCode\":\"1\",\"value\":0.903,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2020-10-01T14:14:00Z\",\"qcFlagCode\":\"1\",\"value\":3.825,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2020-10-01T20:12:00Z\",\"qcFlagCode\":\"1\",\"value\":2.793,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2020-10-02T02:18:00Z\",\"qcFlagCode\":\"1\",\"value\":7.661,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}, {\"eventDate\":\"2020-10-02T06:21:00Z\",\"qcFlagCode\":\"1\",\"value\":3.62,\"timeSeriesId\":\"5cebf1e23d0f4a073c4bc0e1\",\"reviewed\":true}] ");
            i
        })
    .await
        .start_mock_server();


    let mut configuration = chs::apis::configuration::Configuration::new();
    configuration.base_path = pact.path("/tides").to_string();
    let body = "tide 07755 2020/10/1";
    let result = tides::request::handle_request(body, &configuration).await;
    let expected = [
        "Tides for Port Moody (07755) on 2020/10/01 - PDT",
        "01:08: 0.9m",
        "07:14: 3.8m",
        "13:12: 2.8m",
        "19:18: 7.7m",
        "23:21: 3.6m",
    ].join("\n");
    assert_eq!(result, expected);
}
