# Canadian Tides vis SMS

The Department of Fisheries and Oceans publishes OpenAPI specs for their [water level web services](https://tides.gc.ca/en/web-services-offered-canadian-hydrographic-service).

I've generated an API in a sibling directory called `chs`, and this repo is used to generate a Cloudflare Worker to interact with it.

# Generating the `chs` crate

* Install [openapi-generator-cli](https://github.com/OpenAPITools/openapi-generator-cli)

* Generate the client:

```bash
curl --silent -L https://api-iwls.dfo-mpo.gc.ca/v3/api-docs/v1 > chs-openapi.json
openapi-generator-cli generate -g rust -o chs -i chs-openapi.json --package-name chs
```

# Usage

Send an SMS to 778-400-BOAT:

* `station search <name>`: Will respond with a list of codes corresponding to the station. This uses the DFO's IDs for the stations, so you could use their online tools to find the stations as well.
* `station info <code>`: Returns information on the station.
* `tide <station> [date]`: Returns the tides for that station. Date can be blank, `today`/`tomorrow`/`yesterday` or in `YYYY/MM/DD` format.

# Local Testing

```bash
wrangler dev
curl -X POST -d "From=5551234567&To=5551234567&Body=station info 07755" http://localhost:8787/
```
